﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TradeLogisticsPlus.Models;
using TradeLogisticsPlus.ViewModels;
using Microsoft.EntityFrameworkCore;


namespace TradeLogisticsPlus.Controllers
{
    public class InvoicesController : Controller
    {
        private TradeLogisticsPlusContext db;
        public InvoicesController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        // получение всех накладных пользователя
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> MyInvoices()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View(user);
        }
        
    }
}
