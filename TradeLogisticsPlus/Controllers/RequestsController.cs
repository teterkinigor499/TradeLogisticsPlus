﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TradeLogisticsPlus.Models;
using TradeLogisticsPlus.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace TradeLogisticsPlus.Controllers
{
    public class RequestsController : Controller
    {
        private TradeLogisticsPlusContext db;
        public RequestsController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        // получение всех заявок пользователя
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> MyRequests()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);

            if (user.TypeUserId == 1)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }


            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            // получаем статусы заявок
            ViewBag.StatusesRequest = db.StatusesRequests;

            // получаем список товаров, которые есть в заявках пользователя
            List<CommodityInRequest> CommoditysInRequestsUser = new List<CommodityInRequest>();

            foreach (Request request in ViewBag.RequestsUser)
            {
                foreach (CommodityInRequest commodityInRequest in db.CommoditysInRequests.ToList())
                {
                    if (request.Id == commodityInRequest.RequestId)
                    {
                        CommoditysInRequestsUser.Add(commodityInRequest);
                    }
                }
            }

            ViewBag.CommoditysInRequestsUser = CommoditysInRequestsUser;
            return View(user);
        }
        // добавление товаров в заявку
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CommodityInRequest(int id)
        {
            // запоминаем заявку 
            ViewBag.RequestId = id;

            ViewBag.RequestName = db.Requests.FirstOrDefault(u => u.Id == id).Name;

            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddCommodityInRequest(RegisterCommodityInRequest model)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (ModelState.IsValid)
            {
                // проверка на существование связи заявки с товарами
                CommodityInRequest registerCommodityInRequest = await db.CommoditysInRequests.FirstOrDefaultAsync(u => u.RequestId == model.RequestId && u.GoodsId == model.CommodityID &&  (db.Commoditys.FirstOrDefault(p=>p.Id == u.GoodsId)).UserId == user.Id);
                if (registerCommodityInRequest != null)
                {
                    ModelState.AddModelError("", "Товар уже существует в данной заявке");
                    return RedirectToAction("MyRequests", "Requests");

                }
                // если адрес не существует, то добавляем в БД
                else if (registerCommodityInRequest == null)
                {
                    if (model.CommodityID == 0)
                    {
                        ModelState.AddModelError("", "Вы не выбрали товар");
                        return RedirectToAction("MyRequests", "Requests");

                    }
                    db.CommoditysInRequests.Add(new CommodityInRequest
                    {
                        RequestId = model.RequestId,
                        GoodsId = model.CommodityID,
                        Amount = model.Amount,
                        Price = model.Price,
                        Comment = model.Comment
                    });

                    // сохраняем изменения в БД
                    await db.SaveChangesAsync();

                    return RedirectToAction("MyRequests", "Requests");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректные параметры");
                    return RedirectToAction("MyRequests", "Requests");
                }
            }
            return View();
        }
        // регистрация новой заявки
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> RegisterRequest()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (user.TypeUserId == 1)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();


            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RegisterRequest(RegisterRequest model)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (ModelState.IsValid)
            {
                // проверка на существование заявки в личном списке товаров
                Request request = await db.Requests.FirstOrDefaultAsync(u => u.Name == model.NameRequest && u.ObjectId == model.FacilityId && u.UserId == user.Id);
                if (request != null)
                {
                    ModelState.AddModelError("", "Заявка уже существует в вашем списке");
                    return RedirectToAction("RegisterRequest", "Requests");
                }
                // если адрес не существует, то добавляем в БД
                else if (request == null)
                {
                    if (model.FacilityId == 0)
                    {
                        ModelState.AddModelError("", "Вы не выбрали объект");
                        return RedirectToAction("RegisterRequest", "Requests");
                    }
                    db.Requests.Add(new Request
                    {
                        UserId = user.Id,
                        Name = model.NameRequest,
                        Urgency = model.Urgency,
                        DateOfDelivery = model.DateOfDelivery,
                        DateOfCreation = DateTime.Now,
                        StatusRequestId = 1,
                        ObjectId = model.FacilityId
                    });

                    // сохраняем изменения в БД
                    await db.SaveChangesAsync();

                    return RedirectToAction("MyRequests", "Requests");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректное наименование продукта");
                    return RedirectToAction("RegisterRequest", "Requests");
                }
            }
            return View();
        }
        // реализация удаления заявки
        [HttpPost]
        [Authorize]
        public ActionResult RequestDelete(int id)
        {
            // проверка на удаление тем пользователем, чьи товары
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            // поиск заявки
            Request request = db.Requests.FirstOrDefault(c => c.Id == id);
            // приверка возможности удаления
            if (request != null && user.Id == request.UserId)
            {
                // проверка статуса (новая или закрыта)
                if(request.StatusRequestId == 1 || request.StatusRequestId == 4) {

                    // удаляем все связи с продуктами
                    foreach(CommodityInRequest commodityInRequest in db.CommoditysInRequests)
                    {
                        if(request.Id == commodityInRequest.RequestId)
                        {
                            db.CommoditysInRequests.Remove(commodityInRequest);
                        }
                    }

                    db.Requests.Remove(request);
                }
                else
                {
                    ModelState.AddModelError("", "Данная заявка в работе или опубликована");
                    return RedirectToAction("MyRequests", "Requests");
                }
                db.SaveChanges();
            }
            return RedirectToAction("MyRequests", "Requests");
        }
        // реализация публикации заявки
        [HttpPost]
        [Authorize]
        public ActionResult PublishRequest(int id)
        {
            // проверка на публиацию заявки нужным пользователем
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            // поиск заявки
            Request request = db.Requests.FirstOrDefault(c => c.Id == id);
            // приверка возможности публикации - она есть / не в работе / не закрыта
            if (request != null && user.Id == request.UserId && request.StatusRequestId != 3 && request.StatusRequestId != 4)
            {
                    request.StatusRequestId = 2;
                    db.Requests.Update(request);
                    db.SaveChanges();
                return RedirectToAction("MyRequests", "Requests");
            }
            else
            {
                ModelState.AddModelError("", "Данная заявка в работе или уже опубликована");
                return RedirectToAction("MyRequests", "Requests");
            }
            
        }
        // реализация снятия с публикации заявки
        [HttpPost]
        [Authorize]
        public ActionResult DePublishRequest(int id)
        {
            // проверка на публиацию заявки нужным пользователем
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            // поиск заявки
            Request request = db.Requests.FirstOrDefault(c => c.Id == id);
            // приверка возможности публикации - она есть / не в работе / не закрыта

            if (request != null && user.Id == request.UserId && request.StatusRequestId == 2)
            {
                request.StatusRequestId = 1;
                db.Requests.Update(request);
                db.SaveChanges();
                return RedirectToAction("MyRequests", "Requests");
            }
            else if (request.StatusRequestId == 2 || request.StatusRequestId == 3 || request.StatusRequestId == 4)
            {
                ModelState.AddModelError("", "Данная заявка опубликована или в работе или закрыта");
                return RedirectToAction("MyRequests", "Requests");
            }
            else
            {
                ModelState.AddModelError("", "Заявка не найдена");
                return RedirectToAction("MyRequests", "Requests");
            }

        }
        // реализация поиска заявок
        [HttpGet]
        [Authorize]
        public ActionResult SearchRequests()
        {
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (user.TypeUserId == 2)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = db.TypesUsers.FirstOrDefault(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.Commoditys = db.Commoditys;

            // получаем список всех адресов
            ViewBag.Addresses = db.Addresses;

            // получаем список всех объектов
            ViewBag.Facilitys = db.Facilitys;

            // получаем список всех заявок пользователей по типу
            
            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }


            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();


            // получаем список товаров, которые есть в заявках
            ViewBag.CommoditysInRequests = db.CommoditysInRequests;
            // получаем список всех опубликованных заявок
            ViewBag.PublisRequests = db.Requests.Where(u => u.StatusRequestId == 2).ToList();
            // получаем список секторов затрат
            ViewBag.CostSections = db.CostSections;

            return View();
        }
        // реализация взятия заявки в работу
        [Authorize]
        [HttpPost]
        public ActionResult ToTakeRequest(int id)
        {
            // поиск заявки
            Request request = db.Requests.FirstOrDefault(c => c.Id == id);
            // определение исполнителя
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);

            request.ExecutorId = user.Id;
            request.StatusRequestId = 3;
            db.Requests.Update(request);
            db.SaveChanges();
            return RedirectToAction("MyToTakeRequests", "Requests");

        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> MyToTakeRequests()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (user.TypeUserId == 2)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }

            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = db.TypesUsers.FirstOrDefault(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.Commoditys = db.Commoditys;

            // получаем список всех адресов
            ViewBag.Addresses = db.Addresses;

            // получаем список всех объектов
            ViewBag.Facilitys = db.Facilitys;

            // получаем статусы заявок
            ViewBag.StatusesRequest = db.StatusesRequests;
            
            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }


            // получаем список товаров, которые есть в заявках пользователя
            List<CommodityInRequest> CommoditysInRequestsUser = new List<CommodityInRequest>();

            foreach (Request request in ViewBag.RequestsUser)
            {
                foreach (CommodityInRequest commodityInRequest in db.CommoditysInRequests.ToList())
                {
                    if (request.Id == commodityInRequest.RequestId)
                    {
                        CommoditysInRequestsUser.Add(commodityInRequest);
                    }
                }
            }
            ViewBag.CommoditysInRequestsUser = CommoditysInRequestsUser;

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> InfoRequest(int id)
        {
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);


            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров пользователя
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов пользователя
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов пользователя
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();


            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }
            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            // подучение типа пользователя
            ViewBag.TypeUser = db.TypesUsers.FirstOrDefault(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.Commoditys = db.Commoditys;

            // получаем список всех адресов
            ViewBag.Addresses = db.Addresses;

            // получаем список всех объектов
            ViewBag.Facilitys = db.Facilitys;

            ViewBag.Units = db.Units;
            ViewBag.CostSections = db.CostSections;

            // поиск заявки 
            ViewBag.Request  = db.Requests.FirstOrDefault(u => u.Id == id);

            ViewBag.CommoditysInRequest = db.CommoditysInRequests.Where(u => u.RequestId == id).ToList();

            return View();
        }
        
        // реализация просмотра создателя заявки
        [HttpGet]
        [Authorize]
        public ActionResult CreaterRequest(int id)
        {
            // поиск создателя 
            User CreaterRequest = db.Users.FirstOrDefault(p=>p.Id == db.Requests.FirstOrDefault(u => u.Id == id).UserId);

            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);

            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // выравнивание даты рождения
            string birtchDay;

            birtchDay = (Convert.ToString(user.BirthDay)).Replace(" 0:00:00", "");
            ViewBag.BirtchDay = birtchDay;

            // подучение типа пользователя
            ViewBag.TypeUser = db.TypesUsers.FirstOrDefault(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View(CreaterRequest);
        }

        [Authorize]
        [HttpGet]
        public ActionResult AddressesRequest(int id)
        {
            // поиск создателя 
            User CreaterRequest = db.Users.FirstOrDefault(p => p.Id == db.Requests.FirstOrDefault(u => u.Id == id).UserId);

            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);

            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // выравнивание даты рождения
            string birtchDay;

            birtchDay = (Convert.ToString(user.BirthDay)).Replace(" 0:00:00", "");
            ViewBag.BirtchDay = birtchDay;

            // подучение типа пользователя
            ViewBag.TypeUser = db.TypesUsers.FirstOrDefault(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов в заявке
            ViewBag.AddressesUser = db.Addresses.Where(p => p.ObjectId == db.Requests.FirstOrDefault(u => u.Id == id).ObjectId).ToList();

            // получаем список всех объектов 
            ViewBag.FacilitysUser = db.Facilitys;

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ToRegisterInvoice(int id)
        {
            // поиск заявки
            Request request = db.Requests.FirstOrDefault(c => c.Id == id);
            // определение исполнителя
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            // поис накладной 
            Invoice invoice = db.Invoices.FirstOrDefault(u => u.RequestId == id);

            // список всех товаров, которые есть в заявке (для подсчёта общей стоимости)

            List<CommodityInRequest> CommoditysInRequestsUser = new List<CommodityInRequest>();

            foreach (CommodityInRequest commodityInRequest in db.CommoditysInRequests.ToList())
            {
                if (request.Id == commodityInRequest.RequestId)
                {
                    CommoditysInRequestsUser.Add(commodityInRequest);
                }
            }

            if (invoice == null)
            {
                db.Invoices.Add(new Invoice
                {
                    ExecutorId = user.Id,
                    StatusInvalidId = 1,
                    RequestId = id,
                    GeneralPrice = invoice.СountlPrice(CommoditysInRequestsUser),
                    UserId = db.Requests.FirstOrDefault(u => u.Id == id).UserId
                });
            }

            return RedirectToAction("MyInvoices", "Invoices");

        }
    }
}
