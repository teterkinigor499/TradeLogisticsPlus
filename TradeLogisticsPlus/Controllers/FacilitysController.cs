﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TradeLogisticsPlus.Models;
using TradeLogisticsPlus.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace TradeLogisticsPlus.Controllers
{
    public class FacilitysController : Controller
    {
        private TradeLogisticsPlusContext db;
        public FacilitysController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> MyFacilitys()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            // получаем типы объектов
            ViewBag.TypesFacilitys = db.TypesFacilitys;

            return View();
        }
        // регистрация новой заявки
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> RegisterFacility()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (user.TypeUserId == 1)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            // получаем типы объектов
            ViewBag.TypesFacilitys = db.TypesFacilitys;

            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RegisterFacility(RegisterFacility model)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (ModelState.IsValid)
            {
                Facility facility = await db.Facilitys.FirstOrDefaultAsync(u => u.Name == model.NameFacility && u.UserId == user.Id);
                // проверка на существование объекта в личном списке товаров
                if (facility != null)
                {
                    ModelState.AddModelError("", "Объект уже существует в вашем списке");
                    return RedirectToAction("RegisterFacility", "Facilitys");
                }
                // если товар с таким Name не существует, то добавляем в БД
                else if (facility == null)
                {
                    db.Facilitys.Add(new Facility
                    {
                        UserId = user.Id,
                        Name = model.NameFacility,
                        TypeFacilityId = model.TypeFacilityId
                    });

                    // сохраняем изменения в БД
                    await db.SaveChangesAsync();

                    return RedirectToAction("MyFacilitys", "Facilitys");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректное наименование объекта");
                    return RedirectToAction("RegisterFacility", "Facilitys");
                }
            }
            return View();
        }
        // реализация удаления объекта
        [Authorize]
        [HttpPost]
        public ActionResult FacilityDelete(int id)
        {
            // проверка на удаление тем пользователем, чьи товары
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            // поиск объекта
            Facility facility = db.Facilitys.FirstOrDefault(c => c.Id == id);
            // поиск адресов с данным объектом 
            List<Address> addressFacility = db.Addresses.Where(p => p.UserId == user.Id && p.ObjectId == facility.Id).ToList();

            // поиск заявок с данным объектом
            List<Request> requests = db.Requests.Where(u => u.ObjectId == id && u.UserId == user.Id).ToList();
            if (requests.Count != 0)
            {
                ModelState.AddModelError("", "У вас есть заявка, связанная с данным объектом");
                return RedirectToAction("MyFacilitys", "Facilitys");
            }
            else
            {// проверка на существование данного объекта у пользователя
                if (facility != null && user.Id == facility.UserId)
                {
                    db.Facilitys.Remove(facility);
                    foreach (Address AddressFacility in addressFacility)
                    {
                        db.Addresses.Remove(AddressFacility);
                    }

                    db.SaveChanges();
                }
                return RedirectToAction("MyFacilitys", "Facilitys");
            }
        }
    }
}
