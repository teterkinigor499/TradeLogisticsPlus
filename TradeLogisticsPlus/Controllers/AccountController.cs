﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using TradeLogisticsPlus.ViewModels;
using TradeLogisticsPlus.Models;
using TradeLogisticsPlus.ImportantModels;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;


namespace TradeLogisticsPlus.Controllers
{
    public class AccountController : Controller
    {
        private TradeLogisticsPlusContext db;
        public AccountController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        // при обращении возвращаем представление
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        // при ответе от пользователя обрабатываем данные
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginUser model)
        {
            if (ModelState.IsValid)
            {
                string hash = SHA512hash.GetHash(model.Password);
                // проверка существования пользователя по логину или email + паролю (u.Email == model.Email || u.Phone == model.Email)
                User user = await db.Users.FirstOrDefaultAsync(u => (u.Email == model.Email || u.Login == model.Email) && u.Password == SHA512hash.GetHash(model.Password));
                if (user != null)
                {
                    await Authenticate(model.Email); // аутентифицируем пользователя

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректный логин или пароль");
            }
            return View(model);
        }
        // при обращении возвращаем представление
        [HttpGet]
        public IActionResult Register()
        {
            ViewBag.TypesUsers = db.TypesUsers;
            return View();
        }
        // при ответе от пользователя обрабатываем данные
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Register(RegisterUser model)
        {
            if (ModelState.IsValid)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email || u.Login == model.Login);
                // если пользователь с таким Email и с таким Login не существует, то добавляем в БД
                if (user == null)
                {
                    byte[] AvatarData = null;
                    // считываем переданный файл в массив байтов
                    if (model.Avatar != null)
                    {
                        using (var binaryReader = new BinaryReader(model.Avatar.OpenReadStream()))
                        {
                            AvatarData = binaryReader.ReadBytes((int)model.Avatar.Length);
                        }
                    }

                    db.Users.Add(new User
                    {
                        Email = model.Email,
                        Password = SHA512hash.GetHash(model.Password),
                        SurName = model.SurName,
                        Name = model.Name,
                        MiddleName = model.MiddleName,
                        PositionHeld = model.PositionHeld,
                        TypeUserId = model.TypeUserId,
                        Phone = model.Phone,
                        BirthDay = model.BirthDay,
                        Avatar = AvatarData,
                        Login = model.Login
                    });
                    // сохраняем изменения в БД
                    await db.SaveChangesAsync();

                    await Authenticate(model.Email); // сразу аутентифицируем пользователя

                    EmailService emailServise = new EmailService();
                    // отправка на почту приветствия
                    await emailServise.SendToMessage(model.Email, "Регистрация на сервисе Trade Logistics Plus", "<strong>Поздравляем! " + model.Name + ", Вы успешно зарегистрированы!</strong> Сервис Trade Logistics Plus будет уведомлять вас о ходе исполнения заявок. <hr />С уважением, команда Trade Logistics Plus. <br /> <br />Email: <a href=\"mailto:tradelogisticsplus@gmail.com\">tradelogisticsplus@gmail.com</a><br /> тел: +7(929) 601-08-92");

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректный логин или пароль");
                    return RedirectToAction("Register", "Account");
                }
            }
            return View(model);
        }

        private async Task Authenticate(string userEmail)
        {
            // создадим claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType,userEmail)
            };
            // создадим объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }
        [Authorize]
        public async Task<IActionResult> Profile()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // выравнивание даты рождения
            string birtchDay;

            birtchDay = (Convert.ToString(user.BirthDay)).Replace(" 0:00:00", "");
            ViewBag.BirtchDay = birtchDay;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View(user);
        }
    }
}
