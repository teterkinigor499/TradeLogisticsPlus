﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TradeLogisticsPlus.Models;
using TradeLogisticsPlus.ViewModels;
using Microsoft.EntityFrameworkCore;


namespace TradeLogisticsPlus.Controllers
{
    public class AddressesController : Controller
    {
        private TradeLogisticsPlusContext db;
        public AddressesController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        // получение всех накладных пользователя
        [Authorize]
        public async Task<IActionResult> MyAddresses()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);
            
            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов 
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View(user);
        }
        // регистрация нового адреса
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> RegisterAddress()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (user.TypeUserId == 1)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RegisterAddress(RegisterAddress model)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (ModelState.IsValid)
            {
                // проверка на существование адреса в личном списке товаров
                Address address = await db.Addresses.FirstOrDefaultAsync(u => u.Locality == model.Locality && u.Street == model.Street && u.House == model.House && u.UserId == user.Id);
                if (address != null)
                {
                    ModelState.AddModelError("", "Адрес уже существует в вашем списке");
                    return RedirectToAction("RegisterAddress", "Addresses");
                }
                // если адрес не существует, то добавляем в БД
                else if (address == null)
                {
                    if (model.FacilityId == 0)
                    {
                        ModelState.AddModelError("", "Вы не выбрали объект");
                        return RedirectToAction("RegisterAddress", "Addresses");
                    }
                    db.Addresses.Add(new Address
                    {
                        ObjectId = model.FacilityId, // непрямая запись в FacilityId
                        UserId = user.Id,
                        zipCode = model.zipCode,
                        District = model.District,
                        Building = model.Building,
                        Letter = model.Letter,
                        Street = model.Street,
                        Stock = model.Stock,
                        Locality = model.Locality,
                        Region = model.Region,
                        House = model.House
                    });

                    // сохраняем изменения в БД
                    await db.SaveChangesAsync();

                    return RedirectToAction("MyAddresses", "Addresses");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректное наименование продукта");
                    return RedirectToAction("RegisterAddress", "Addresses");
                }
            }
            return View();
        }
        // реализация удаления адреса
        [HttpPost]
        [Authorize]
        public ActionResult AddressDelete(int id)
        {
            // проверка на удаление тем пользователем, чьи товары
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            // поиск адреса
            Address addressFacility = db.Addresses.FirstOrDefault(c => c.Id == id);

            // поиск заявок с данным адресом
            List<Request> requests = db.Requests.Where(p => p.ObjectId == db.Facilitys.FirstOrDefault(u=>u.Id == addressFacility.ObjectId).Id).ToList();
            if (requests.Count != 0) {
                ModelState.AddModelError("", "У вас есть заявка, связанная с данным адресом");
                return RedirectToAction("MyAddresses", "Addresses");
            }
            else
            {
                // проверка на существование данного адреса у пользователя
                if (addressFacility != null && user.Id == addressFacility.UserId)
                {
                    db.Addresses.Remove(addressFacility);
                    db.SaveChanges();
                }
                return RedirectToAction("MyAddresses", "Addresses");
            }
            
        }
    }
}
