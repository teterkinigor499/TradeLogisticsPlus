﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TradeLogisticsPlus.Models;
using TradeLogisticsPlus.ViewModels;
using Microsoft.EntityFrameworkCore;


namespace TradeLogisticsPlus.Controllers
{
    public class CommoditysController : Controller
    {
        private TradeLogisticsPlusContext db;
        public CommoditysController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        [Authorize]
        [HttpGet]
        // получаем необходимые товары
        public async Task<IActionResult> MyCommoditys()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();
            
            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            ViewBag.Units = db.Units;
            ViewBag.CostSections = db.CostSections;

            return View(user);
        }
        // регистрация нового товара
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> RegisterCommodity()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (user.TypeUserId == 1)
            {
                ModelState.AddModelError("", "Вам недоступен данный раздел");
                return RedirectToAction("Index", "Home");
            }
            ViewBag.avatar = user.Avatar;
            ViewBag.login = user.Login;

            // подучение типа пользователя
            ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

            // получаем список всех товаров 
            ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех адресов
            ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех объектов
            ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

            // получаем список всех заявок пользователей по типу
            if (ViewBag.TypeUser.Id == 2)
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
            }
            else
            {
                ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
            }

            // получаем список всех накладных 
            // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();

            ViewBag.Units = db.Units;
            ViewBag.CostSections = db.CostSections;

            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> RegisterCommodity(RegisterCommodity model)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
            if (ModelState.IsValid)
            {
                Commodity commodity = await db.Commoditys.FirstOrDefaultAsync(u => u.Name == model.NameCommodity && u.UserId == user.Id);
                // проверка на существование товара в личном списке товаров
                if (commodity != null)
                {
                    ModelState.AddModelError("", "Товар уже существует в вашем списке");
                    return RedirectToAction("RegisterCommodity", "Commoditys");
                }
                // если товар с таким Name не существует, то добавляем в БД
                else if (commodity == null)
                {
                    if (model.CostSectionId == 0 || model.UnitId == 0)
                    {
                        ModelState.AddModelError("", "Вы не указали параметры товара");
                        return RedirectToAction("RegisterCommodity", "Commoditys");
                    }
                    db.Commoditys.Add(new Commodity
                    {
                        UserId = user.Id,
                        Name = model.NameCommodity,
                        UnitId = model.UnitId,
                        CostSectionId = model.CostSectionId
                    });

                    // сохраняем изменения в БД
                    await db.SaveChangesAsync();

                    return RedirectToAction("MyCommoditys", "Commoditys");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректное наименование товара");
                    return RedirectToAction("RegisterCommodity", "Commoditys");
                }
            }
            return View();
        }
        // реализация удаления товара 
        [Authorize]
        [HttpPost]
        public ActionResult CommodityDelete(int id)
        {
            // проверка на удаление тем пользователем, чьи товары
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);


            // проверка на содержание товара в какой либо заявке
            CommodityInRequest commodityInRequest = db.CommoditysInRequests.FirstOrDefault(u => u.GoodsId == id);

            if (commodityInRequest == null)
            {
                Commodity commoditys = db.Commoditys.FirstOrDefault(c => c.Id == id);
                if (commoditys != null && user.Id == commoditys.UserId)
                {
                    db.Commoditys.Remove(commoditys);
                    db.SaveChanges();
                }
                return RedirectToAction("MyCommoditys", "Commoditys");
            }
            else
            {
                ModelState.AddModelError("", "Данный товар есть в заявке");
                return RedirectToAction("MyCommoditys", "Commoditys");
            }
        }
    }
}
