﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TradeLogisticsPlus.Models;
using Microsoft.EntityFrameworkCore;

namespace TradeLogisticsPlus.Controllers
{
    public class HomeController : Controller
    {
        private TradeLogisticsPlusContext db;
        public HomeController(TradeLogisticsPlusContext context)
        {
            db = context;
        }
        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
                ViewBag.avatar = user.Avatar;
                ViewBag.login = user.Login;

           
                // подучение типа пользователя
                ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

                // получаем список всех заявок пользователей по типу

                ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех товаров 
                ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех адресов
                ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех объектов
                ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех заявок пользователей по типу
                if (ViewBag.TypeUser.Id == 2)
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
                }
                else
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
                }

                // получаем список всех накладных 
                // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();
            }
            return View();
        }
        public async Task<IActionResult> About()
        {
            ViewData["Message"] = "Информация о системе";
            if (User.Identity.IsAuthenticated)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
                ViewBag.avatar = user.Avatar;
                ViewBag.login = user.Login;

                // получаем список всех заявок взятых в работу 
                ViewBag.RequestsExecutor = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();

                // подучение типа пользователя
                ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

                // получаем список всех товаров 
                ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех адресов
                ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех объектов
                ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех заявок пользователей по типу
                if (ViewBag.TypeUser.Id == 2)
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
                }
                else
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
                }

                // получаем список всех накладных 
                // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();
            }
            return View();
        }
        public async Task<IActionResult> Contact()
        {
            ViewData["Message"] = "Здесь вы найдёте информацию о создателях сервиса";
            if (User.Identity.IsAuthenticated)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
                ViewBag.avatar = user.Avatar;
                ViewBag.login = user.Login;

                // получаем список всех заявок взятых в работу 
                ViewBag.RequestsExecutor = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();

                // подучение типа пользователя
                ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

                // получаем список всех товаров 
                ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех адресов
                ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех объектов
                ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех заявок пользователей по типу
                if (ViewBag.TypeUser.Id == 2)
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
                }
                else
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
                }

                // получаем список всех накладных 
                // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();
            }
            return View();
        }
        public async Task<IActionResult> Error()
        {
            if (User.Identity.IsAuthenticated)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name || u.Login == User.Identity.Name);
                ViewBag.avatar = user.Avatar;
                ViewBag.login = user.Login;

                // получаем список всех заявок взятых в работу 
                ViewBag.RequestsExecutor = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();

                // подучение типа пользователя
                ViewBag.TypeUser = await db.TypesUsers.FirstOrDefaultAsync(u => u.Id == user.TypeUserId);

                // получаем список всех товаров 
                ViewBag.CommoditysUser = db.Commoditys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех адресов
                ViewBag.AddressesUser = db.Addresses.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех объектов
                ViewBag.FacilitysUser = db.Facilitys.Where(p => p.UserId == user.Id).ToList();

                // получаем список всех заявок пользователей по типу
                if (ViewBag.TypeUser.Id == 2)
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.UserId == user.Id).ToList();
                }
                else
                {
                    ViewBag.RequestsUser = db.Requests.Where(p => p.ExecutorId == user.Id).ToList();
                }

                // получаем список всех накладных 
                // ViewBag.InvalidsUser = db.Invalids.Where(p => p.UserId == user.Id).ToList();
            }
            return View();
        }
    }
}
