﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TradeLogisticsPlus.Models;

namespace TradeLogisticsPlus.Migrations
{
    [DbContext(typeof(TradeLogisticsPlusContext))]
    partial class TradeLogisticsPlusContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TradeLogisticsPlus.Models.Address", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("Building");

                    b.Property<string>("District");

                    b.Property<int?>("FacilityId");

                    b.Property<int>("House");

                    b.Property<string>("Letter");

                    b.Property<string>("Locality");

                    b.Property<int>("ObjectId");

                    b.Property<string>("Region");

                    b.Property<int?>("Stock");

                    b.Property<string>("Street");

                    b.Property<int>("UserId");

                    b.Property<int?>("zipCode");

                    b.HasKey("Id");

                    b.HasIndex("FacilityId");

                    b.HasIndex("UserId");

                    b.ToTable("Addresses");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Commodity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CostSectionId");

                    b.Property<string>("Name");

                    b.Property<int>("UnitId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CostSectionId");

                    b.HasIndex("UnitId");

                    b.HasIndex("UserId");

                    b.ToTable("Commoditys");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.CommodityInRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<float>("Amount");

                    b.Property<string>("Comment");

                    b.Property<int?>("CommodityId");

                    b.Property<int>("GoodsId");

                    b.Property<float>("Price");

                    b.Property<int>("RequestId");

                    b.HasKey("Id");

                    b.HasIndex("CommodityId");

                    b.HasIndex("RequestId");

                    b.ToTable("CommoditysInRequests");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.CostSection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("CostSections");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Facility", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("TypeFacilityId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("TypeFacilityId");

                    b.HasIndex("UserId");

                    b.ToTable("Facilitys");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Invoice", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ExecutorId");

                    b.Property<float>("GeneralPrice");

                    b.Property<int>("RequestId");

                    b.Property<int>("StatusInvalidId");

                    b.Property<int?>("StatusInvoicesId");

                    b.Property<int>("UserId");

                    b.Property<byte[]>("Сheque");

                    b.HasKey("Id");

                    b.HasIndex("RequestId")
                        .IsUnique();

                    b.HasIndex("StatusInvoicesId");

                    b.ToTable("Invoices");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Request", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateOfCreation");

                    b.Property<DateTime>("DateOfDelivery");

                    b.Property<int>("ExecutorId");

                    b.Property<int?>("FacilityId");

                    b.Property<string>("Name");

                    b.Property<int>("ObjectId");

                    b.Property<int>("StatusRequestId");

                    b.Property<bool>("Urgency");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("FacilityId");

                    b.HasIndex("StatusRequestId");

                    b.HasIndex("UserId");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.StatusInvoices", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("StatusesInvoices");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.StatusRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("StatusesRequests");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.TypeFacility", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("TypesFacilitys");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.TypeUser", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("TypesUsers");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Unit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.ToTable("Units");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<byte[]>("Avatar");

                    b.Property<DateTime>("BirthDay");

                    b.Property<string>("Email");

                    b.Property<string>("Login");

                    b.Property<string>("MiddleName");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<string>("Phone");

                    b.Property<string>("PositionHeld");

                    b.Property<string>("SurName");

                    b.Property<int>("TypeUserId");

                    b.HasKey("Id");

                    b.HasIndex("TypeUserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Address", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.Facility")
                        .WithMany("AddressesFacilitys")
                        .HasForeignKey("FacilityId");

                    b.HasOne("TradeLogisticsPlus.Models.User")
                        .WithMany("AddressesFacilitys")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Commodity", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.CostSection")
                        .WithMany("Commoditys")
                        .HasForeignKey("CostSectionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TradeLogisticsPlus.Models.Unit")
                        .WithMany("Commoditys")
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TradeLogisticsPlus.Models.User")
                        .WithMany("Commoditys")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.CommodityInRequest", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.Commodity")
                        .WithMany("CommoditysInRequests")
                        .HasForeignKey("CommodityId");

                    b.HasOne("TradeLogisticsPlus.Models.Request")
                        .WithMany("CommoditysInRequests")
                        .HasForeignKey("RequestId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Facility", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.TypeFacility")
                        .WithMany("Facilitys")
                        .HasForeignKey("TypeFacilityId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TradeLogisticsPlus.Models.User")
                        .WithMany("Facilitys")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Invoice", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.Request")
                        .WithOne("Invoices")
                        .HasForeignKey("TradeLogisticsPlus.Models.Invoice", "RequestId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TradeLogisticsPlus.Models.StatusInvoices")
                        .WithMany("Invoices")
                        .HasForeignKey("StatusInvoicesId");
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.Request", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.Facility")
                        .WithMany("Requests")
                        .HasForeignKey("FacilityId");

                    b.HasOne("TradeLogisticsPlus.Models.StatusRequest")
                        .WithMany("Requests")
                        .HasForeignKey("StatusRequestId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TradeLogisticsPlus.Models.User")
                        .WithMany("Requests")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TradeLogisticsPlus.Models.User", b =>
                {
                    b.HasOne("TradeLogisticsPlus.Models.TypeUser")
                        .WithMany("Users")
                        .HasForeignKey("TypeUserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
