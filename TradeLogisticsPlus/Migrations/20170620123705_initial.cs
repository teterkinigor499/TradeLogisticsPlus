﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TradeLogisticsPlus.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CostSections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostSections", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StatusesInvoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusesInvoices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StatusesRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusesRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypesFacilitys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypesFacilitys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypesUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypesUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Avatar = table.Column<byte[]>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Login = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    PositionHeld = table.Column<string>(nullable: true),
                    SurName = table.Column<string>(nullable: true),
                    TypeUserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_TypesUsers_TypeUserId",
                        column: x => x.TypeUserId,
                        principalTable: "TypesUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Commoditys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CostSectionId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UnitId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commoditys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Commoditys_CostSections_CostSectionId",
                        column: x => x.CostSectionId,
                        principalTable: "CostSections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Commoditys_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Commoditys_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Facilitys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    TypeFacilityId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facilitys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Facilitys_TypesFacilitys_TypeFacilityId",
                        column: x => x.TypeFacilityId,
                        principalTable: "TypesFacilitys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Facilitys_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Building = table.Column<int>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    FacilityId = table.Column<int>(nullable: true),
                    House = table.Column<int>(nullable: false),
                    Letter = table.Column<string>(nullable: true),
                    Locality = table.Column<string>(nullable: true),
                    ObjectId = table.Column<int>(nullable: false),
                    Region = table.Column<string>(nullable: true),
                    Stock = table.Column<int>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    zipCode = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Facilitys_FacilityId",
                        column: x => x.FacilityId,
                        principalTable: "Facilitys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Addresses_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateOfCreation = table.Column<DateTime>(nullable: false),
                    DateOfDelivery = table.Column<DateTime>(nullable: false),
                    ExecutorId = table.Column<int>(nullable: false),
                    FacilityId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ObjectId = table.Column<int>(nullable: false),
                    StatusRequestId = table.Column<int>(nullable: false),
                    Urgency = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Requests_Facilitys_FacilityId",
                        column: x => x.FacilityId,
                        principalTable: "Facilitys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Requests_StatusesRequests_StatusRequestId",
                        column: x => x.StatusRequestId,
                        principalTable: "StatusesRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Requests_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommoditysInRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<float>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    CommodityId = table.Column<int>(nullable: true),
                    GoodsId = table.Column<int>(nullable: false),
                    Price = table.Column<float>(nullable: false),
                    RequestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommoditysInRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommoditysInRequests_Commoditys_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commoditys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommoditysInRequests_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExecutorId = table.Column<int>(nullable: false),
                    GeneralPrice = table.Column<float>(nullable: false),
                    RequestId = table.Column<int>(nullable: false),
                    StatusInvalidId = table.Column<int>(nullable: false),
                    StatusInvoicesId = table.Column<int>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    Сheque = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Invoices_StatusesInvoices_StatusInvoicesId",
                        column: x => x.StatusInvoicesId,
                        principalTable: "StatusesInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_FacilityId",
                table: "Addresses",
                column: "FacilityId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UserId",
                table: "Addresses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Commoditys_CostSectionId",
                table: "Commoditys",
                column: "CostSectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Commoditys_UnitId",
                table: "Commoditys",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Commoditys_UserId",
                table: "Commoditys",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CommoditysInRequests_CommodityId",
                table: "CommoditysInRequests",
                column: "CommodityId");

            migrationBuilder.CreateIndex(
                name: "IX_CommoditysInRequests_RequestId",
                table: "CommoditysInRequests",
                column: "RequestId");

            migrationBuilder.CreateIndex(
                name: "IX_Facilitys_TypeFacilityId",
                table: "Facilitys",
                column: "TypeFacilityId");

            migrationBuilder.CreateIndex(
                name: "IX_Facilitys_UserId",
                table: "Facilitys",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_RequestId",
                table: "Invoices",
                column: "RequestId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_StatusInvoicesId",
                table: "Invoices",
                column: "StatusInvoicesId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_FacilityId",
                table: "Requests",
                column: "FacilityId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_StatusRequestId",
                table: "Requests",
                column: "StatusRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_UserId",
                table: "Requests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TypeUserId",
                table: "Users",
                column: "TypeUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "CommoditysInRequests");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Commoditys");

            migrationBuilder.DropTable(
                name: "Requests");

            migrationBuilder.DropTable(
                name: "StatusesInvoices");

            migrationBuilder.DropTable(
                name: "CostSections");

            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropTable(
                name: "Facilitys");

            migrationBuilder.DropTable(
                name: "StatusesRequests");

            migrationBuilder.DropTable(
                name: "TypesFacilitys");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "TypesUsers");
        }
    }
}
