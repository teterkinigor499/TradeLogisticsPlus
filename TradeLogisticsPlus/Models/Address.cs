﻿namespace TradeLogisticsPlus.Models
{
    public class Address
    {
        // ключи
        public int Id { get; set; } // id адреса - первичный ключ
        public int UserId { get; set; } //auto/ id пользователя - создателя
        public int ObjectId { get; set; } // id объекта, которому принадлежит адрес

        // данные модели
        public int? zipCode { get; set; } // почтовый индекс (если известен)
        public string Region { get; set; } // область
        public string District { get; set; } // район
        public string Locality { get; set; } // населённый пункт (город, деревня, село)
        public string Street { get; set; } // улица
        public int House { get; set; } // дом
        public string Letter { get; set; } // литера (если есть)
        public int? Building { get; set; } // корпус (если есть)
        public int? Stock { get; set; } // строение (если есть)
    }
}
