﻿using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class CostSection
    {
        // ключи
        public int Id { get; set; } // id сектора затрат - первичный ключ
        public List<Commodity> Commoditys { get; set; } // список товаров, которые принадлежат данному сектору затрат (связь один ко многим)

        // данные модели
        public string Name { get; set; } // наименование раздела затрат
       
    }
}
