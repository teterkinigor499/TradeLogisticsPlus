﻿using Microsoft.EntityFrameworkCore;

namespace TradeLogisticsPlus.Models
{
    public class TradeLogisticsPlusContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Facility> Facilitys { get; set; }
        public DbSet<TypeFacility> TypesFacilitys { get; set; }
        public DbSet<CommodityInRequest> CommoditysInRequests { get; set; }
        public DbSet<Commodity> Commoditys { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<CostSection> CostSections { get; set; }
        public DbSet<TypeUser> TypesUsers { get; set; }
        public DbSet<StatusRequest> StatusesRequests { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<StatusInvoices> StatusesInvoices { get; set; }

        public TradeLogisticsPlusContext(DbContextOptions<TradeLogisticsPlusContext> options)
            : base(options)
        {

        }
    }
}
