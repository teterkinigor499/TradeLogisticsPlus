﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradeLogisticsPlus.Models
{
    public class TypeUser
    {
        // ключи
        public int Id { get; set; } // id типа - первичный ключ
        public List<User> Users { get; set; } // список пользователей с данным типом (связь один ко многим)

        // данные модели
        public string Name { get; set; } // наименование типа пользователя

        
    }
}
