﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradeLogisticsPlus.Models
{
    public class StatusInvoices
    {
        // ключи
        public int Id { get; set; } // id статуса - первичный ключ
        public List<Invoice> Invoices { get; set; } // список накладных 

        // данные модели
        public string Name { get; set; } // наименование статуса заявки
    }
}
