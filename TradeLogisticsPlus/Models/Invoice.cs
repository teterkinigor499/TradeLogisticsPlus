﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TradeLogisticsPlus.Models
{
    public class Invoice
    {

        // ключи
        public int Id { get; set; } // id - накладной
        public int UserId { get; set; } // id - создателя
        public int ExecutorId { get; set; } // id - исполнителя
        public int RequestId { get; set; }

        public int StatusInvalidId { get; set; } // статус накладной

        // данные модели
        public float GeneralPrice { get; set; }
        public byte[] Сheque { get; set; }


        // дополнительные методы
        public float СountlPrice (List<CommodityInRequest> CommoditysInRequests) {
            float generalPrice = 0;
            foreach(CommodityInRequest commodityInRequest in CommoditysInRequests)
            {
                generalPrice += (commodityInRequest.Amount) * commodityInRequest.Price;
            }
            return generalPrice;
        }
    }
}
