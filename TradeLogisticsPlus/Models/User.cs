﻿using System;
using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class User
    {
        // ключи
        public int Id { get; set; } // id - пользователя

        public int TypeUserId { get; set; } // id типа пользователя

        public List<Address> AddressesFacilitys { get; set; } // список адресов пользователя
        public List<Request> Requests { get; set; } // список заявок пользователя
        public List<Commodity> Commoditys { get; set; } // список товаров пользователя
        public List<Facility> Facilitys { get; set; } // список объектов пользователя

        // данные модели
        public string Login { get; set; } // догин
        public string Email { get; set; } // email
        public string Password { get; set; } // пароль
        public string SurName { get; set; } // фамилия
        public string Name { get; set; } // имя
        public string MiddleName { get; set; } // отчество
        public string PositionHeld { get; set; } // занимаемая должность
        public string Phone { get; set; } // номер телефона
        public DateTime BirthDay { get; set; } // дата рождения
        public byte[] Avatar { get; set; } // аватар
    }
}
