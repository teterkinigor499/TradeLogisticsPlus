﻿using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class Commodity
    {
        // ключи
        public int Id { get; set; } // id товара - первичный ключ
        public int UserId { get; set; } // id пользователя - создателя
        public int CostSectionId { get; set; } // id сектора затрат
        public int UnitId { get; set; } // id ед. изм

        // связь многие ко многим 
        public List<CommodityInRequest> CommoditysInRequests { get; set; }
        public Commodity()
        {
            CommoditysInRequests = new List<CommodityInRequest>(); // список товаров в заявках
        }

        // данные модели
        public string Name { get; set; } // наименование товара
    }
}
