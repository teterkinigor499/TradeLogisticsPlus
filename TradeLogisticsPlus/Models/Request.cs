﻿using System;
using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class Request
    {
        // ключи
        public int Id { get; set; } // id заявки - первичный ключ

        public int UserId { get; set; } //auto/ id пользователя - создателя
        public int ExecutorId { get; set; }
        public int ObjectId { get; set; } // id объекта, на котором создана заявка
        public int StatusRequestId { get; set; } // id статуса заявки

        public Invoice Invoices { get; set; } // связь с накладной

        // связь многие ко многим через CommodityInRequest 
        public List<CommodityInRequest> CommoditysInRequests { get; set; } 
        public Request()
        {
            CommoditysInRequests = new List<CommodityInRequest>(); // список товаров в заявках
        }

        // данные модели
        public string Name { get; set; } // наименование заявки
        public DateTime DateOfCreation { get; set; } // дата создания заявки
        public DateTime DateOfDelivery { get; set; } // требуемая дата поставки
        public bool Urgency { get; set; } // срочность выполнения

    }
}
