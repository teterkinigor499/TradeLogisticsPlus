﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradeLogisticsPlus.Models
{
    public class StatusRequest
    {
        // ключи
        public int Id { get; set; } // id статуса - первичный ключ
        public List<Request> Requests { get; set; } // список заявок с данным статусом (связь один ко многим)

        // данные модели
        public string Name { get; set; } // наименование статуса заявки
    }
}
