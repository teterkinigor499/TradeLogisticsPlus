﻿using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class Facility
    {
        // ключи 
        public int Id { get; set; } // id объекта - первичный ключ

        public int UserId { get; set; } // id пользователя - создателя
        public int TypeFacilityId { get; set; } // id типа объекта

        public List<Address> AddressesFacilitys { get; set; } // список адресов, которые принадлежат объекту (связь один ко многим)
        public List<Request> Requests { get; set; } // список заявок, которые сделаны на данном объекте (связь один ко многим)

        // данные модели
        public string Name { get; set; } // наименование объекта
    }
}
