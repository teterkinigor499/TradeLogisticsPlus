﻿using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class Unit
    {
        // ключи
        public int Id { get; set; } // первичный ключ
        public List<Commodity> Commoditys { get; set; } // список товаров с данной ед. изм. (связь один ко многим)

        // данные модели
        public string Name { get; set; } // наименование ед. изм.
        public string Value { get; set; } // краткое обозначение ед. изм.


    }
}
