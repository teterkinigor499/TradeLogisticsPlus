﻿namespace TradeLogisticsPlus.Models
{
    public class CommodityInRequest
    {
        // ключи
        public int Id { get; set; } // id связи товара с заявкой - первичный ключ

        public int GoodsId { get; set; } // id товара
        public int RequestId { get; set; } // id заявки

        // данные модели
        public float Amount { get; set; } // количество товара
        public float Price { get; set; } // стоимость товара
        public string Comment { get; set; } // комментарий по товару
    }
}
