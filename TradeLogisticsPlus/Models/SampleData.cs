﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace TradeLogisticsPlus.Models
{
    public static class SampleData
    {
        public static void Initializ(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<TradeLogisticsPlusContext>();

            if (!context.StatusesInvoices.Any())
            {
                context.StatusesInvoices.AddRange(
                    new StatusInvoices
                    {
                        Name = "Сформирована"
                    },
                    new StatusInvoices
                    {
                        Name = "Оплачена"
                    }
                );
            }
            if (!context.Units.Any())
            {
                // добавляем единицы измерения
                context.Units.AddRange(
                    // Единицы измерения длины
                    new Unit
                    {
                        Name = "Длина в мм",
                        Value = "мм"
                    },
                    new Unit
                    {
                        Name = "Длина в см",
                        Value = "см"
                    },
                    new Unit
                    {
                        Name = "Длина в дм",
                        Value = "дм"
                    },
                    new Unit
                    {
                        Name = "Длина в м",
                        Value = "м"
                    },
                    new Unit
                    {
                        Name = "Длина в км",
                        Value = "км"
                    },
                    // Единицы измерения площади
                    new Unit
                    {
                        Name = "Площадь в мм2",
                        Value = "мм2"
                    },
                    new Unit
                    {
                        Name = "Площадь в см2",
                        Value = "см2"
                    },
                    new Unit
                    {
                        Name = "Площадь в дм2",
                        Value = "дм2"
                    },
                    new Unit
                    {
                        Name = "Площадь в м2",
                        Value = "м2"
                    },
                    new Unit
                    {
                        Name = "Площадь в км2",
                        Value = "км2"
                    },
                    // Единицы измерения объёма
                    new Unit
                    {
                        Name = "Объём в мм3",
                        Value = "мм2"
                    },
                    new Unit
                    {
                        Name = "Объём в см3",
                        Value = "см2"
                    },
                    new Unit
                    {
                        Name = "Объём в дм3",
                        Value = "дм2"
                    },
                    new Unit
                    {
                        Name = "Объём в л",
                        Value = "л"
                    },
                    new Unit
                    {
                        Name = "Объём в м3",
                        Value = "м2"
                    },
                    new Unit
                    {
                        Name = "Объём в км3",
                        Value = "км2"
                    },
                    // единицы измерения количества
                    new Unit
                    {
                        Name = "Количество в шт",
                        Value = "шт",
                    }
                );
                context.SaveChanges();
            }
            // добавление статусов заявки
            if (!context.StatusesRequests.Any())
            {
                context.StatusesRequests.AddRange(
                    new StatusRequest
                    {
                        Name = "Новая"
                    },
                    new StatusRequest
                    {
                        Name = "Опублекована"
                    },
                    new StatusRequest
                    {
                        Name = "В работе"
                    },
                    new StatusRequest
                    {
                        Name = "Закрыта"
                    }
                );
            }
            if (!context.TypesUsers.Any())
            {
                context.TypesUsers.AddRange(
                    new TypeUser
                    {
                        Name = "Поставщик"
                    },
                    new TypeUser
                    {
                        Name = "Покупатель"
                    }
                );
                context.SaveChanges();
            }
            // добавляем разделы затрат
            if (!context.CostSections.Any()) {
                context.CostSections.AddRange(
                    new CostSection
                    {
                        Name = "Механические системы"
                    },
                    new CostSection
                    {
                        Name = "Электрические системы"
                    },
                    new CostSection
                    {
                        Name = "Строительные товары"
                    },
                    new CostSection
                    {
                        Name = "Метизы и расходники"
                    },
                    new CostSection
                    {
                        Name = "Спецодежда и СИЗ"
                    },
                    new CostSection
                    {
                        Name = "Инструмент"
                    },
                    new CostSection
                    {
                        Name = "Канцелярия"
                    },
                    new CostSection
                    {
                        Name = "Другое"
                    }
                );
                context.SaveChanges();
            }
            // добавляем  группы затрат
            
            // заполним типы объектов
            if (!context.TypesFacilitys.Any())
            {
                context.TypesFacilitys.AddRange(
                    new TypeFacility
                    {
                        Name = "Жилая недвижимость"
                    },
                    new TypeFacility
                    {
                        Name = "Коммерческая недвижимость"
                    },
                    new TypeFacility
                    {
                        Name = "Промышленная недвижимость"
                    },
                    new TypeFacility
                    {
                        Name = "Социально-культурная промышленность"
                    },
                    new TypeFacility
                    {
                        Name = "Другое"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
