﻿using System.Collections.Generic;

namespace TradeLogisticsPlus.Models
{
    public class TypeFacility
    {
        // ключи
        public int Id { get; set; } // id типа - первичный ключ
        public List<Facility> Facilitys { get; set; } // список объектов с данным типом (связь один ко многим)

        // данные модели
        public string Name { get; set; } // наименование типа объекта
    }
}
