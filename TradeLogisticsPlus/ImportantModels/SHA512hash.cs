﻿using System.Security.Cryptography;
using System.Text;

namespace TradeLogisticsPlus.ImportantModels
{
    // класс для преобразование пароля пользователя в результат хэш-функции с солью
    public static class SHA512hash
    {
        public static string GetHash(string inputPassword)
        {
            byte[] inputData = Encoding.GetEncoding("utf-8").GetBytes(inputPassword); // перевод пароля пользователя в массив бит
            byte[] key = Encoding.GetEncoding("utf-8").GetBytes("Belka"); // соль
            byte[] outputData; // результирующий пароль

            using (var sha512 = new HMACSHA512(key))
            {
                outputData = sha512.ComputeHash(inputData); // получение хэщированного пароля с солью
            }
            // перевод полученного массива бит в строку
            var sb = new StringBuilder();

            foreach (byte b in outputData) {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
    }
}
