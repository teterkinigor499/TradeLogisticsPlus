﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace TradeLogisticsPlus.ImportantModels
{
    public class EmailService
    {
        public async Task SendToMessage(string userEmail, string subject, string message) {
            var EmailMessage = new MimeMessage();

            EmailMessage.From.Add(new MailboxAddress("Trade Logistics Plus", "tradelogisticsplus@gmail.com"));
            EmailMessage.To.Add(new MailboxAddress("", userEmail));
            EmailMessage.Subject = subject;
            EmailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, false);  // подключение к серверу
                await client.AuthenticateAsync("tradelogisticsplus@gmail.com", "F1%Belka4_"); // аутентификация
                await client.SendAsync(EmailMessage); // отправка сообщения
                await client.DisconnectAsync(true); // отключение
            }
        }
    }
}
