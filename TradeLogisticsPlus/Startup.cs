﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TradeLogisticsPlus.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace TradeLogisticsPlus
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // подключение к хранилищу данных
            /*var connectionString = new SqlConnectionStringBuilder();
            connectionString.DataSource = "tcp:igor-web.database.windows.net";
            connectionString.InitialCatalog = "TradeLogisticsPlus";
            connectionString.Encrypt = true;
            connectionString.TrustServerCertificate = false;
            connectionString.UserID = "Belka383";
            connectionString.Password = "499482403Server";
            */
            // подключение к локальному серверу
            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<TradeLogisticsPlusContext>(options => options.UseSqlServer(connection));
            // подключение к удалённому серверу
            //services.AddDbContext<TradeLogisticsPlusContext>(options => options.UseSqlServer(connectionString.ToString()));
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            SampleData.Initializ(app.ApplicationServices);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                // определим название схемы аутентификации
                AuthenticationScheme = "Cookies",
                // установим относительный путь, по которому аноним будет перенаправляться
                LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login"),
                // преверка пользователя на аутентификацию при каждом запросе
                AutomaticAuthenticate = true,
                // неавторизованных пользователь при попытке перейти куда-то будет перебрасываться на авторизацию
                AutomaticChallenge = true
            });

            app.UseStaticFiles();


            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
           
        }
    }
}
