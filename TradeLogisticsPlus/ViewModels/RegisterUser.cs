﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TradeLogisticsPlus.ViewModels
{
    public class RegisterUser
    {
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Вы не указали логин")]
        [Display(Name = "Login")]
        [DataType(DataType.Text)]
        public string Login { get; set; }


        [Required(ErrorMessage = "Вы не указали пароль")]
        [StringLength(16, MinimumLength = 8, ErrorMessage = "Длина пароля должна быть от 8 до 16 символов")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [Display(Name = "ConfirmPassword")]
        public string ConfirmPassword { get; set; } // пароль для проверки

        [Required(ErrorMessage = "Укажите фамилию")]
        [DataType(DataType.Text)]
        public string SurName { get; set; } // фамилия


        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Укажите имя")]
        public string Name { get; set; } // имя

        [DataType(DataType.Text)]
        public string MiddleName { get; set; } // отчество

        [Required(ErrorMessage = "Вы не указали занимаемую должность")]
        [DataType(DataType.Text)]
        public string PositionHeld { get; set; } // занимаемая должность

        [Required(ErrorMessage = "Вы не указали кто вы")]
        public int TypeUserId { get; set; } // тип пользователя

        [Required(ErrorMessage = "Вы не ввели телефон")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; } // номер телефона

        [Required(ErrorMessage = "Вы не ввели дату рождения")]
        [DataType(DataType.DateTime)]
        public DateTime BirthDay { get; set; } // дата рождения

        [Required(ErrorMessage = "Вы не выбрали аватар")]
        public IFormFile Avatar { get; set; } // аватар
    }
}
