﻿using System.ComponentModel.DataAnnotations;

namespace TradeLogisticsPlus.ViewModels
{
    public class LoginUser
    {
        [Required(ErrorMessage = "Не указан Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
