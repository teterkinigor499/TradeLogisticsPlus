﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TradeLogisticsPlus.ViewModels
{
    public class RegisterFacility
    {
        [Required(ErrorMessage = "Вы не указали наименование")]
        [Display(Name = "NameFacility")]
        public string NameFacility { get; set; } // наименование объекта

        [Required(ErrorMessage = "Вы не указали тип объекта")]
        [DataType(DataType.Text)]
        public int TypeFacilityId { get; set; } // id типа объекта

    }
}
