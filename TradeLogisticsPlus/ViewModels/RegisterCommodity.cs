﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TradeLogisticsPlus.ViewModels
{
    public class RegisterCommodity
    {
        [Required(ErrorMessage = "Вы не указали наименование")]
        [Display(Name = "NameCommodity")]
        public string NameCommodity { get; set; }

        [Required(ErrorMessage = "Вы не указали сектор затрат")]
        [Display(Name = "CostSectionsId")]
        public int CostSectionId { get; set; }

 
        [Required(ErrorMessage = "Вы не указали единицы измерения")]
        [Display(Name = "Unit")]
        public int UnitId { get; set; }
    }
}
