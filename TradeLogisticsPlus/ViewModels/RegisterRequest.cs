﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TradeLogisticsPlus.ViewModels
{
    public class RegisterRequest
    {
        [Display(Name = "FacilityId")]
        public int FacilityId { get; set; } // id объекта, которому принадлежит адрес

        [Required(ErrorMessage = "Вы не указали наименование")]
        [Display(Name = "NameRequest")]
        public string NameRequest { get; set; } // наименование заявки

        [Required(ErrorMessage = "Вы не указали желаемую дату исполнения")]
        [Display(Name = "DateOfDelivery")]
        [DataType(DataType.DateTime)]
        public DateTime DateOfDelivery { get; set; } // требуемая дата поставки

        [Required(ErrorMessage = "Вы не указали срочность")]
        [Display(Name = "Urgency")]
        public bool Urgency { get; set; } // срочность выполнения
    }
}
