﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TradeLogisticsPlus.ViewModels
{
    public class RegisterAddress
    {
        [Required(ErrorMessage = "Вы не указали объект")]
        [Display(Name = "FacilityId")]
        public int FacilityId { get; set; } // id объекта, которому принадлежит адрес

        [Required(ErrorMessage = "Вы не указали индекс")]
        [Display(Name = "zipCode")]
        public int zipCode { get; set; } // почтовый индекс (если известен)

        [Required(ErrorMessage = "Вы не указали область")]
        [Display(Name = "Region")]
        public string Region { get; set; } // область (если есть)

        [Required(ErrorMessage = "Вы не указали район")]
        [Display(Name = "District")]
        public string District { get; set; } // район

        [Required(ErrorMessage = "Вы не указали населённый пункт")]
        [Display(Name = "Locality")]
        public string Locality { get; set; } // населённый пункт (город, деревня, село)

        [Required(ErrorMessage = "Вы не указали улицу")]
        [Display(Name = "Street")]
        public string Street { get; set; } // улица

        [Required(ErrorMessage = "Вы не указали дом")]
        [Display(Name = "House")]
        public int House { get; set; } // дом

        [Display(Name = "Letter")]
        public string Letter { get; set; } // литера (если есть)

        [Display(Name = "Building")]
        public int? Building { get; set; } // корпус (если есть)

        [Display(Name = "Stock")]
        public int? Stock { get; set; } // строение (если есть)
    }
}
