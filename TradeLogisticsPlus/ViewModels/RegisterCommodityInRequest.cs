﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace TradeLogisticsPlus.ViewModels
{
    public class RegisterCommodityInRequest
    {
        // предоставляется выбор 
        public int CommodityID { get; set; } // id товара
        // устанавливается автоматически
        public int RequestId { get; set; } // id заявки

        [Required(ErrorMessage = "Вы не указали количество")]
        [Display(Name = "Amount")]
        public float Amount { get; set; } // количество товара

        [Required(ErrorMessage = "Вы не указали приемлемую цену")]
        [Display(Name = "Price")]
        public float Price { get; set; } // стоимость товара

        [Display(Name = "Comment")]
        public string Comment { get; set; } // комментарий по товару
    }
}
